import { BrowserModule } from '@angular/platform-browser';
import { NgModule , NO_ERRORS_SCHEMA} from '@angular/core';
import { Ng5SliderModule } from 'ng5-slider';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import {AppService} from "./app.service";
import {CarouselItemListComponent} from "./app.carouselItemList.component";
import {PageNotFoundComponent} from "./not-found.component";
import {AppRoutingModule} from "./app-routing.module";
import { MDBBootstrapModule } from 'angular-bootstrap-md';


@NgModule({
  declarations: [
    AppComponent,
    CarouselItemListComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    Ng5SliderModule,
    HttpClientModule,
    AppRoutingModule,
    MDBBootstrapModule.forRoot(),
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
