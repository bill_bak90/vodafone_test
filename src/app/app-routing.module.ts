import { NgModule }              from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';
import {CarouselItemListComponent} from "./app.carouselItemList.component";
import {AppComponent} from "./app.component";
import {PageNotFoundComponent} from "./not-found.component";

const appRoutes: Routes = [
  { path: 'appComponent',                    component: AppComponent },
  { path: 'carouselItemListComponent',       component: CarouselItemListComponent },
  { path: '',                                redirectTo: '/appComponent', pathMatch: 'full' },
  { path: '**',                              component: CarouselItemListComponent  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
