import {Component, OnInit} from '@angular/core';
import {AppService} from "./app.service";
import { CarouselModule, WavesModule, ButtonsModule } from 'angular-bootstrap-md'

@Component({
  selector:"app-carousel",
  templateUrl: './app.carouselItemList.html',
  styleUrls: ['./app.component.scss']
})

export class CarouselItemListComponent implements  OnInit{

  public myInterval: number = 10000;
  public activeSlideIndex: number = 0;
  public noWrapSlides:boolean = false;
  public articles:Array<Object>;
  public tariffs:Array<Object>;
  public variants : Array<Object>;
  public tariffs_group_name:Array<Object>;
  constructor(private appService : AppService) {}

  ngOnInit(){

    this.appService.getJSON().subscribe(data => {

      let articles = data.result.articles.articles;
      let variants = [];
      // let tariff_group_names  = [];


      for (let i = 0; i < articles.length; ++i) {
        for (let j = 0; j < articles[i].variants.length; ++j) {
               variants.push( articles[i].variants[j]);
           }
      }
      this.variants = variants;

      // for ( let i = 0; i < variants.length; ++i) {
      //   for (let j = 0; j < variants[i].tariffPrices.length; ++j ) {
      //       tariff_group_names.push(variants[i].tariffPrices);
      //   }
      // }
      // this.tariffs_group_name= tariff_group_names;

    });
  }

  arrayOne(n: number): any[] {
    return Array(n);
  }

}
