import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AppService {

  constructor(private http: HttpClient) {

    var obj;
    this.getJSON().subscribe(data => obj=data, error => console.log(error));

  }
  public getJSON(): Observable<any> {
    return this.http.get("./assets/login.json")
  }

}
