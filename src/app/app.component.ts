import {Component, OnInit} from '@angular/core';
import { Options } from 'ng5-slider';
import {AppService} from "./app.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit{

  value: number = 55;
  options: Options = {
    floor: 0,
    ceil: 100,
    showTicks: true,
    tickStep: 10 ,
  };
  constructor() {}

  ngOnInit(){
  }





}
